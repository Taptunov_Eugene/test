package org.example.service;

import com.google.gson.Gson;
import org.example.dto.Message;
import org.example.dto.User;

import java.io.IOException;
import java.io.InputStream;

public class MessageService {
    private static Gson gson = new Gson();

    public static String commandHandle(Message message) {
        switch (message.getCommand()) {
            case ROOMS: {
                try (InputStream stream = MessageService.class.getResourceAsStream("/rooms.json")) {
                    return "{\"command\":\"ROOMS\", \"rooms\":" + new String(stream.readAllBytes()) + "}";
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case USERS:
                try (InputStream stream = MessageService.class.getResourceAsStream("/rooms.json")) {
                    return "{\"command\":\"USERS\", \"users\":" + new String(stream.readAllBytes()) + "}";
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case CURRENT_USER:
                return "{\"command\":\"CURRENT_USER\", \"currentUser\":" + gson.toJson(new User("12", "Valik", "w@i.ua", "Valik")) + "}";
        }
        return "No command";
    }
}
