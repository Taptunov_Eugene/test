package org.example.util;


import org.example.model.User;

import java.sql.*;

public class RegUtil {
    private static Statement stmt;
    private static PreparedStatement preparedStatement;
    private static ResultSet rs;
    private static Connection connection = null;


    public static boolean createPerson(User acount) throws SQLException {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users" +
                            "(firstName, surName, login, password, photoUrl, statusonline)" +
                            "VALUES ( ?, ?, ?, ?,  ?,?) "

            );
            statement.setString(1, acount.getFirstName());
            statement.setString(2, acount.getSurName());
            statement.setString(3, acount.getLogin());
            statement.setString(4, acount.getPassword());
            statement.setString(5, acount.getPhotoUrl());
            statement.setBoolean(6, true);
            statement.execute();
            System.out.println("Create: " + acount.getLogin());

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPerson(String login, String password) {
        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement(
                            "SELECT * FROM users WHERE login =" + "'" + login + "'" + " AND password =" + "'" + password + "'"

                    );
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet == null) {
                return true;
            }
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return true;
        }
    }
}
