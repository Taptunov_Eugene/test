package org.example.ws;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import org.example.dto.LoginMessage;
import org.example.dto.TokenResponse;
import org.example.jwtToken.TokenGenerator;

import java.nio.charset.StandardCharsets;

public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private Gson gson = new Gson();
    private static final String WS = "/ws?token=";
    WebSocketServerHandshaker handshaker;

    @Override
    public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest httpRequest) {
        String uri = httpRequest.uri();
        if (uri.startsWith(WS)) {

            String substring = uri.substring(uri.indexOf("=") + 1 );
            HttpHeaders headers = httpRequest.headers();

            if (TokenGenerator.isTokenValid(substring)) {
                System.out.println("Connection : " + headers.get("Connection"));
                System.out.println("Upgrade : " + headers.get("Upgrade"));
                if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION)) &&
                        "WebSocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                    //Adding new handler to the existing pipeline to handle WebSocket Messages
                    ctx.pipeline().replace(this, "websocketHandler", new WebSocketHandler());
                    System.out.println("WebSocketHandler added to the pipeline");
                    System.out.println("Opened Channel : " + ctx.channel());
                    System.out.println("Handshaking....");
                    //Do the Handshake to upgrade connection from HTTP to WebSocket protocol
                    handleHandshake(ctx, httpRequest);
                    System.out.println("Handshake is done");
                }
            } else {
                badResponceGenerate(ctx);
            }
        }
        switch (uri) {
            case "/login": {
                LoginMessage loginMessage = new Gson().fromJson(httpRequest.content().toString(StandardCharsets.UTF_8), LoginMessage.class);
                String token = null;
                try {
                    token = TokenGenerator.generateToken(loginMessage.getEmail(), loginMessage.getPassword());
                } catch (IllegalArgumentException e) {
                    badResponceGenerate(ctx);
                }
                String json = gson.toJson(new TokenResponse(new TokenResponse.Tokens(token, token), "12"));

                ByteBuf responseBytes = ctx.alloc().buffer();
                responseBytes.writeBytes(json.getBytes());
                FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, responseBytes);
                httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
                httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
                httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

                ctx.channel().writeAndFlush(httpResponse);

                break;
            }

        }

    }

    private void badResponceGenerate(ChannelHandlerContext ctx) {
        FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
        httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

        ctx.channel().writeAndFlush(httpResponse);
    }

    /* Do the handshaking for WebSocket request */
    protected void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }
    }

    protected String getWebSocketURL(HttpRequest req) {
        System.out.println("Req URI : " + req.getUri());
        String url = "ws://" + req.headers().get("Host") + req.getUri();
        System.out.println("Constructed URL : " + url);
        return url;
    }
}
