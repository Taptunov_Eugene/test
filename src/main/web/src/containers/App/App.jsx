import React from 'react';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {Box} from '@material-ui/core';
import {Redirect, Route, Switch} from 'react-router';
import {useSessionStorage} from 'react-use';

import {useAuthBase} from '../../hooks/useAuthBase';
import {RouteWithCheckAuth} from '../../router/RouteWithCheckAuth';
import {Menu} from '../../components';
import {People} from '../People/People';
import {Channels} from '../Channels/Channels';
import {About} from '../About/About';
import {selectCurrentUser} from './selectors';


export const App = () => {
    const dispatch = useDispatch();
    const {name} = useSelector(selectCurrentUser, shallowEqual);

    const [value, setValue] = useSessionStorage('userAuthData');

    // useEffect(() => {
    //   if (!value) {
    //     dispatch(actionAppLogOut());
    //     dispatch(push('/sign-in'));
    //   }
    // }, [value]);

    return (
        <Box>
            <Menu
                title={name}
                handleLogOut={() => {
                    setValue(undefined);
                }}
            >
                <Switch>
                    <RouteWithCheckAuth
                        exact
                        path="/people"
                        redirectPath="/sign-in"
                        useAuthBase={useAuthBase}
                        component={People}
                    />
                    <RouteWithCheckAuth
                        exact
                        path="/channels"
                        redirectPath="/sign-in"
                        useAuthBase={useAuthBase}
                        component={Channels}
                    />
                    <RouteWithCheckAuth
                        exact
                        path="/about"
                        redirectPath="/sign-in"
                        useAuthBase={useAuthBase}
                        component={About}
                    />

                    <Route component={() => <Redirect to="/people"/>}/>
                </Switch>
            </Menu>
        </Box>
    );
};
