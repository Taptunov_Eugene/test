import React, { useState } from 'react';
import { Box, Toolbar } from '@material-ui/core';
import { /* useDispatch, */ useSelector } from 'react-redux';

import { Chat } from '../../components';

export const Channels = () => {
  // const dispatch = useDispatch();
  // const [roomName, setRoomName] = useState('');
  const [roomId, setRoomId] = useState('');
  const rooms = useSelector((state) => state.rooms);
  const messages = useSelector((state) => state.messages);

  return (
    <Box>
      <Toolbar />
      <h1>Channels</h1>
    </Box>
  );
};
