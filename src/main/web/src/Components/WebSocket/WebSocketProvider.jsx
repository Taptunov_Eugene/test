import React, { createContext } from 'react';
// import { useDispatch } from 'react-redux';

import { WS_BASE } from '../../config.js';

export const WebSocketContext = createContext(null);

export const WebSocketProvider = ({ children }) => {
  let socket;
  let ws;

  // const dispatch = useDispatch();

  const sendMessage = (roomId, message) => {
    const payload = { roomId, data: message };

    socket.send(JSON.stringify(payload));

    // dispatch(sendMessageRequest(payload));
  };

  if (!socket) {
    socket = new WebSocket(WS_BASE);

    socket.onopen = function (e) {
      console.log(e, '[open] Соединение установлено');
      console.log('Отправляем данные на сервер');
      socket.send('Меня зовут Джон');
    };

    socket.onmessage = function (event) {
      console.log(`[message] Данные получены с сервера: ${event.data}`);
    };

    socket.onclose = function (event) {
      if (event.wasClean) {
        console.log(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
      } else {
        // например, сервер убил процесс или сеть недоступна
        // обычно в этом случае event.code 1006
        console.log('[close] Соединение прервано');
      }
    };

    socket.onerror = function (error) {
      console.log(`[error] ${error.message}`);
    };

    ws = {
      socket,
      sendMessage,
    };
  }

  return <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>;
};
